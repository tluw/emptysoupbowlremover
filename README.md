# README #

EmptySoupBowlRemover - A companion plugin to KingKits which lets players consume mushroom soup and not have to worry about dealing with the empty bowls!

### Why? ###

I wasn't able to find any plugins that did what I needed to be done, and that was to remove the empty bowls once a player has consumed a mushroom soup.

### How do I use it? ###

There are 2 ways to get started:
* Download the latest snapshot JAR from the /target/ directory and put the JAR into your /plugins/ directory and start your Spigot-based server
* Clone the whole repo and build it yourself (only need Spigot API 1.10 shaded) and put the resulting JAR into your /plugins/ directory and start your Spigot-based server

### Contribution guidelines ###

Fork this plugin to your own repo and make the desired changes and open a Pull Request back to my branch.

### Author ###

sperling@tluw.net

### License ###
This software is released under the MIT License

Copyright (c) 2016 sperling@tluw.net

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.