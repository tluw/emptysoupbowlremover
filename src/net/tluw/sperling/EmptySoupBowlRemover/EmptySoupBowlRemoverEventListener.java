package net.tluw.sperling.EmptySoupBowlRemover;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class EmptySoupBowlRemoverEventListener implements Listener {
	EmptySoupBowlRemover plugin;
	
	EmptySoupBowlRemoverEventListener(EmptySoupBowlRemover plugin) {
		this.plugin = plugin;
	}

	// Fun fact: There used to be a better way to do this, but that was removed by Spigot a while
	// back when Minecraft updated their code to allow items to be usable in main hand and off-hand
	// situations. So, because of that, we need to use the PlayerInteract event and figure out which
	// hand our target item is in, and THEN do stuff with it. Thanks, Mojang :P
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		// Get an instance of the player associated w/ the event
		Player player = event.getPlayer();
		
		// Get an integer for how many food levels we should be added when a soup is consumed
		Integer intFoodLevelToAdd = (Integer) plugin.getConfig().get("food-level-to-restore");
		
		// Let's only execute if a bunch of conditions are actually true (will use less ticks)
		if(
				(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) &&
				player.getFoodLevel() < 20 &&
				(player.getInventory().getItemInMainHand().getType() == Material.MUSHROOM_SOUP || player.getInventory().getItemInOffHand().getType() == Material.MUSHROOM_SOUP)) {

			// If the player has a mushroom soup in both hands, use the one out of the offhand first
			// Either way, set the in hand item to air first, and then we'll actually add to their food level
			if(player.getInventory().getItemInOffHand().getType() == Material.MUSHROOM_SOUP || (player.getInventory().getItemInMainHand().getType() == Material.MUSHROOM_SOUP && player.getInventory().getItemInOffHand().getType() == Material.MUSHROOM_SOUP)) {
				player.getInventory().setItemInOffHand(new ItemStack(Material.AIR, 1));
			} else {
				player.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
			}
			
			// Update the player's food level
			player.setFoodLevel(Math.min(20, player.getFoodLevel() + intFoodLevelToAdd));

			// Commit the update to the player's inventory
			player.updateInventory();

			// Cancel the event since we're already setting the food level for them
			event.setCancelled(true);
		}
	}
}
