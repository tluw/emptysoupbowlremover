package net.tluw.sperling.EmptySoupBowlRemover;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class EmptySoupBowlRemoverCommandListener implements CommandExecutor {
	EmptySoupBowlRemover plugin;
	
	EmptySoupBowlRemoverCommandListener(EmptySoupBowlRemover plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// /esrbreload - Reloads the config file
		if(command.getName().equalsIgnoreCase("esbrreload")) {
			// Reload the config from disk
			plugin.reloadConfig();
			
			// Tell the caller we've reloaded it
			sender.sendMessage(this.plugin.prefix + "Reloaded configuration");
			
			// return true to let the system know that this command succeeded
			return true;
		}
		// A command that does NOT match what's registered was entered; bad user!
		return false;
	}
}
