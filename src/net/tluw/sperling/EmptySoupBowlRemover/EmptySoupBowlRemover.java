package net.tluw.sperling.EmptySoupBowlRemover;

import java.util.logging.Logger;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class EmptySoupBowlRemover extends JavaPlugin {
	public final Logger logger = Logger.getLogger(this.getName()); // I like loggers!
	public final String prefix = "[EmptySoupBowlRemover] "; // Our output prefix
	
	@Override
	public void onEnable() {
		// Load our configuration
		this.getConfig().options().copyDefaults(true);
		this.saveDefaultConfig();
		
		// Register our reload command
		this.getCommand("esbrreload").setExecutor(new EmptySoupBowlRemoverCommandListener(this));
		
		// Get an instance of PluginManager so we can register our event listener
		PluginManager pluginManager = getServer().getPluginManager();
		
		// Get an instance of this so we can register against it
		EmptySoupBowlRemoverEventListener eventListener = new EmptySoupBowlRemoverEventListener(this);
		
		// Finally, register our eventListener w/ the server's PluginManager
		pluginManager.registerEvents(eventListener, this);

		// Yay! We're enabled!
		this.logger.info(prefix + "enabled!");
	}
	
	@Override
	public void onDisable() {
		// Save our configuration
		this.saveDefaultConfig();
		
		// Tell people that we're disabled
		this.logger.info(prefix + " disabled!");
	}
}
